# Imagen docker base inicial
FROM node:latest

# Crear directorio de trabajo del contenedor Docker
WORKDIR /docker-api

# Copiar archivos de proyecto a directorio Docker
ADD . /docker-api

# Instalar las dependencias del proyecto en produccion
# RUN npm install --production
#                 --only=peoduction (dependendiendo de la version de node)

# Puerto donde se expondrà el contenedor Docker (mismo del servidor node = 3000)
EXPOSE 3010

# Lanzar la aplicacion (appe.js)
# si se desea lanzar un comando que no sea start debe añadirse el elemento "run" en cmd ["npm","run","start"]doc
CMD ["npm", "start"]
